package com.example.airportmonitor.repository

import com.example.airportmonitor.model.Airport
import com.example.airportmonitor.model.Flight
import com.example.airportmonitor.network.ApiService
import com.skydoves.sandwich.ApiResponse
import javax.inject.Inject

/**
 * Fetch data from from the [apiService]
 */
class AirportRepository @Inject constructor(var apiService: ApiService) {

    suspend fun getAirports(): ApiResponse<List<Airport>> {
        return apiService.getAirports()
    }

    suspend fun getFlights(): ApiResponse<List<Flight>> {
        return apiService.getFlights()
    }
}
