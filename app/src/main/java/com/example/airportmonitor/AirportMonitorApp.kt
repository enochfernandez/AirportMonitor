package com.example.airportmonitor

import android.app.Application
import androidx.fragment.app.FragmentManager
import dagger.hilt.android.HiltAndroidApp

/**
 * Application class, run with application lifecycle
 */
@HiltAndroidApp
class AirportMonitorApp: Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            FragmentManager.enableDebugLogging(true)
        }
    }
}
