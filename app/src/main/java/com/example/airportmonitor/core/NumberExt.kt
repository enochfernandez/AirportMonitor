package com.example.airportmonitor.core

//Convert distance from meters to km
fun Float.convertMToKm(): Float {
    return this / 1000
}

//Convert distance from meters to miles
fun Float.convertFromKmToMiles(): Float {
    return this/1.60934f
}

//Convert distance from meters to miles
fun Float.convertMToMiles(): Float {
    return this/1609.344f
}
