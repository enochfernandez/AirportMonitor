package com.example.airportmonitor.core

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * SharedPreferences file to store global settings
 */
@Singleton
class MySharedPreference @Inject constructor(@ApplicationContext context: Context) {
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)


    var distanceUnit: Boolean
        get() = prefs.getBoolean(DISTANCE_UNIT, false)
        set(value) {
            prefs.edit { putBoolean(DISTANCE_UNIT, value) }
        }


    companion object {
        const val DISTANCE_UNIT = "distanceUnit"
    }

}
