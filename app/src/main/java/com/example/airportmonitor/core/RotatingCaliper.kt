package com.example.airportmonitor.core

import kotlin.math.abs
import kotlin.math.sqrt

/**
 * Class used to find the furthest/ maximum distance between points
 * on a co-ordinate plane
 */
class RotatingCaliper {

    fun convexHull(p: Array<Point>): List<Point> {
        if (p.isEmpty()) return emptyList()
        p.sort()
        val h = mutableListOf<Point>()

        // lower hull
        for (pt in p) {
            while (h.size >= 2 && !ccw(h[h.size - 2], h.last(), pt)) {
                h.removeAt(h.lastIndex)
            }
            h.add(pt)
        }

        // upper hull
        val t = h.size + 1
        for (i in p.size - 2 downTo 0) {
            val pt = p[i]
            while (h.size >= t && !ccw(h[h.size - 2], h.last(), pt)) {
                h.removeAt(h.lastIndex)
            }
            h.add(pt)
        }
        h.removeAt(h.lastIndex)
        return h
    }

    /* ccw returns true if the three points make a counter-clockwise turn */
    private fun ccw(a: Point, b: Point, c: Point) =
        ((b.x - a.x) * (c.y - a.y)) > ((b.y - a.y) * (c.x - a.x))

    fun rotatingCaliper(points: Array<Point>): Triple<Point, Point, Double>? {
        val convexHull: ArrayList<Point> = convexHull(points) as ArrayList<Point>
        val n: Int = convexHull.size

        var k = 1
        // Find the farthest vertex
        // from hull[0] and hull[n-1]
        while (absArea(convexHull[n - 1], convexHull[0], convexHull[(k + 1) % n])
            > absArea(convexHull[n - 1], convexHull[0], convexHull[k])) { k++ }

        var res = 0.0
        var furthestDistancePoints: Triple<Point, Point, Double>? = null
        var j = k

        for (i in 0..k) {
            if (i <= k && j < n) {
                res = res.coerceAtLeast(sqrt(dist(convexHull[i], convexHull[j]).toDouble()))
                furthestDistancePoints = Triple(convexHull[i], convexHull[j], res)

                while (j < n && absArea(convexHull[i], convexHull[(i + 1) % n], convexHull[(j + 1) % n]) >
                    absArea(convexHull[i], convexHull[(i + 1) % n], convexHull[j])
                ) { // Update res
                    res = res.coerceAtLeast(sqrt(dist(convexHull[i], convexHull[(j + 1) % n]).toDouble()))
                    furthestDistancePoints = Triple(convexHull[i], convexHull[(j + 1) % n], res)
                    j++
                }
            }
        }
        // Return the result distance
        return furthestDistancePoints
    }

    // Function to find the area
    private fun area(p: Point, q: Point, r: Point): Double {
        // 2*(area of triangle)
        return ((p.y - q.y) * (q.x - r.x)
                - (q.y - r.y) * (p.x - q.x))
    }

    // Function to find the absolute Area
    private fun absArea(
        p: Point,
        q: Point, r: Point
    ): Double {
        // Unsigned area
        // 2*(area of triangle)
        return abs(area(p, q, r))
    }

    // Function to find the distance
    private fun dist(p1: Point, p2: Point): Double {
        // squared-distance b/w
        // p1 and p2 for precision
        return ((p1.x - p2.x) * (p1.x - p2.x)
                + (p1.y - p2.y) * (p1.y - p2.y))
    }
}
