package com.example.airportmonitor.core

/**
 * Class that hold info of location points
 * @param x - this usually represents the latitude
 * @param y - this usually represents the longitute
 * @param pointId - represents the unique Id for the Pont
 */
class Point(val x: Double, val y: Double, val pointId: String = "") : Comparable<Point> {

    override fun compareTo(other: Point) = this.x.compareTo(other.x)

    override fun toString() = "($x, $y)"

    override fun equals(other: Any?): Boolean {
        var other = other as Point
        if (this === other) {
            return true
        }
        return (this.x == other.x)
                && (this.y == other.y)
                && (this.pointId == other.pointId)
    }
}
