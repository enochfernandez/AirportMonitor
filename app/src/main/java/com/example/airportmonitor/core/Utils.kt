package com.example.airportmonitor.core

import com.example.airportmonitor.R
import com.example.airportmonitor.ui.utils.Screens

/**
 * Get random image to be used on Airport Details page
 */
val randomDrawableId = listOf(
    R.drawable.arp1,
    R.drawable.arp2,
    R.drawable.arp3,
    R.drawable.arp4,
    R.drawable.arp5,
    R.drawable.arp6,
    R.drawable.arp7,
    R.drawable.arp8,
    R.drawable.arp9,
).random()

/**
 * Provides the list of screens to be used in navigationBar
 */
val listOfScreens = listOf(
    Screens.AirportMapsScreen,
    Screens.NearbyAirportScreen,
    Screens.SettingsScreen
)
