package com.example.airportmonitor.network

import com.example.airportmonitor.model.Airport
import com.example.airportmonitor.model.Flight
import com.skydoves.sandwich.ApiResponse
import retrofit2.http.GET

/**
 * Interface contract to fetch data from the network
 */
interface ApiService {

    @GET("airports.json")
    suspend fun getAirports(): ApiResponse<List<Airport>>

    @GET("flights.json")
    suspend fun getFlights(): ApiResponse<List<Flight>>
}
