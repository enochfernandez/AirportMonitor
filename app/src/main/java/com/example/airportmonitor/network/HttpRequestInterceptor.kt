package com.example.airportmonitor.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Interceptor class to intercept calls
 */
class HttpRequestInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val request = originalRequest.newBuilder().url(originalRequest.url).build()
        Log.d("HttpRequestInterceptor", request.toString())
        return chain.proceed(request)
    }
}
