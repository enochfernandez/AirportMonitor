package com.example.airportmonitor.ui.viewModels

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.airportmonitor.repository.AirportRepository
import com.example.airportmonitor.ui.utils.AirportsResult
import com.example.airportmonitor.ui.utils.FlightResults
import com.skydoves.sandwich.suspendOnError
import com.skydoves.sandwich.suspendOnFailure
import com.skydoves.sandwich.suspendOnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class AirportsViewModel @Inject constructor(private val airportRepository: AirportRepository): ViewModel() {

    val _airportsResult = mutableStateOf(AirportsResult())

    val _flightsResult = mutableStateOf(FlightResults())

    fun getAirportResultValue(): AirportsResult {
        return _airportsResult.value
    }

    fun getFlightResultValue(): FlightResults {
        return _flightsResult.value
    }

    /**
     * Get all airports from api client
     */
    fun getAllAirports() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = airportRepository.getAirports()
            response.suspendOnSuccess {
                _airportsResult.value = AirportsResult(success = data)
            }.suspendOnError {
                _airportsResult.value = AirportsResult(errorOrFailure = toString())
            }.suspendOnFailure {
                _airportsResult.value = AirportsResult(errorOrFailure = toString())
            }
        }
    }

    /**
     * Get all flights from api client
     */
    fun getAllFlights() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = airportRepository.getFlights()
            response.suspendOnSuccess {
                _flightsResult.value = FlightResults(success = data)
            }.suspendOnError {
                _flightsResult.value = FlightResults(errorOrFailure = toString())
            }.suspendOnFailure {
                _flightsResult.value = FlightResults(errorOrFailure = toString())
            }
        }
    }
}
