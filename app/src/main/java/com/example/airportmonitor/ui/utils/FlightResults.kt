package com.example.airportmonitor.ui.utils

import com.example.airportmonitor.model.Flight

/**
 * Holds data for the success or failure of Flights data retrieval
 */
data class FlightResults(
    val success: List<Flight>? = null,
    val errorOrFailure: String? = null
)
