package com.example.airportmonitor.ui.airportMaps

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.example.airportmonitor.R
import com.example.airportmonitor.core.MySharedPreference
import com.example.airportmonitor.core.convertMToKm
import com.example.airportmonitor.core.convertMToMiles
import com.example.airportmonitor.core.rememberMapViewWithLifecycle
import com.example.airportmonitor.ui.utils.AirportDataTransformPresenter
import com.example.airportmonitor.ui.utils.Screens
import com.example.airportmonitor.ui.utils.distanceFromSelectedAirport
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ktx.awaitMap
import kotlinx.coroutines.launch

@Composable
fun AirportMapsScreen(
    navController: NavHostController,
    presenter: AirportDataTransformPresenter,
    sharedPreference: MySharedPreference,
    bottomBar: @Composable () -> Unit,
    topBar: @Composable () -> Unit
) {
    val airports = presenter.getAirports()
    val furthestPointsOnMap = presenter.getFarthestPointsOnMap()

    val furthestAirport1 = airports?.find {
        it.id == furthestPointsOnMap?.first?.pointId
    }
    val furthestAirport2 = airports?.find {
        it.id == furthestPointsOnMap?.second?.pointId
    }

    val furthestDistance = furthestAirport1.distanceFromSelectedAirport(furthestAirport2)

    val furthestDistanceWithUnit = if (sharedPreference.distanceUnit)
        "${furthestDistance.convertMToMiles()} mi"
    else "${furthestDistance.convertMToKm()} km"

    val mapView = rememberMapViewWithLifecycle()
    val coroutineScope = rememberCoroutineScope()

    var selectedAirport by remember { mutableStateOf(Pair("", "")) }
    Scaffold(
        topBar = topBar,
        bottomBar = bottomBar
    )
    {

        Box {
            AndroidView({ mapView }, modifier = Modifier.clickable {
                navController.navigate(
                    Screens.AirportDetailScreen.route
                )
            }
            ) {
                coroutineScope.launch {
                    val map = it.awaitMap()

                    airports?.let {
                        it.forEach { airport ->
                            val latLong = LatLng(airport.latitude, airport.longitude)
                            val marker = map.addMarker(
                                MarkerOptions()
                                    .position(latLong)
                                    .title(airport.name)
                            )
                            marker.tag = Pair(airport.id, airport.name)
                            if (furthestAirport1?.id == airport.id) {
                                marker.showInfoWindow()
                            }
                        }

                        map.moveCamera(
                            CameraUpdateFactory.newLatLng(
                                LatLng(
                                    furthestAirport1?.latitude ?: 0.0,
                                    furthestAirport1?.longitude ?: 0.0
                                )
                            )
                        )
                    }

                    map.setOnMarkerClickListener { marker ->
                        selectedAirport = marker.tag as Pair<String, String>
                        false
                    }

                }
            }

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(15.dp),
                elevation = 10.dp,
            ) {
                Column(
                    modifier = Modifier.padding(12.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.furthest_distance)
                                + " " + furthestAirport1?.name + ", " + furthestAirport2?.name,
                        style = TextStyle(
                            fontSize = 16.sp
                        ),
                        modifier = Modifier.padding(bottom = 10.dp)

                    )
                    Text(
                        text = stringResource(id = R.string.distance) + " " + furthestDistanceWithUnit,
                    )
                }
            }

            Card(
                modifier = Modifier
                    .clickable {
                        if (selectedAirport.first.isNotEmpty())
                        navController.navigate(
                            Screens.AirportDetailScreen.route + "/${selectedAirport.first}"
                        )
                    }
                    .fillMaxWidth()
                    .padding(top = 15.dp, start = 15.dp, end = 15.dp, bottom = 60.dp)
                    .align(Alignment.BottomCenter),
                elevation = 10.dp,
            ) {
                Column(
                    modifier = Modifier.padding(12.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.see_details),
                        style = TextStyle(
                            fontSize = 16.sp
                        )
                    )
                    Text(text = selectedAirport.second)
                }
            }
        }
    }
}
