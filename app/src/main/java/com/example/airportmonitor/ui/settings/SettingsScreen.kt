package com.example.airportmonitor.ui.settings

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.airportmonitor.R
import com.example.airportmonitor.core.MySharedPreference

@Composable
fun SettingsScreen(
    sharedPreference: MySharedPreference,
    bottomBar: @Composable () -> Unit,
    topBar: @Composable () -> Unit
) {
    Scaffold(topBar = topBar, bottomBar = bottomBar) {

        Column {
            var switched by remember { mutableStateOf(sharedPreference.distanceUnit) }
            val onSwitchedChange: (Boolean) -> Unit = {
                switched = it
                sharedPreference.distanceUnit = it
            }

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.End,
                modifier = Modifier.padding(start = 16.dp)
            ) {

                Text(text = stringResource(id = R.string.settings_distance))
                Switch(checked = switched, onCheckedChange = onSwitchedChange)
            }
        }
    }
}
