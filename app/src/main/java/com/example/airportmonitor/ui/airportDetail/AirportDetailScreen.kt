package com.example.airportmonitor.ui.airportDetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.airportmonitor.R
import com.example.airportmonitor.core.MySharedPreference
import com.example.airportmonitor.core.convertMToKm
import com.example.airportmonitor.core.convertMToMiles
import com.example.airportmonitor.core.randomDrawableId
import com.example.airportmonitor.ui.utils.AirportDataTransformPresenter

@Composable
fun AirportDetailScreen(
    presenter: AirportDataTransformPresenter,
    airportId: String,
    sharedPreference: MySharedPreference,
    topBar: @Composable () -> Unit
) {

    val selectedAirport = presenter.getSingleAirport(airportId)
    val nearestAirport = presenter.getNearestAirportInfo(airportId)
    val nearestAirportDistance = if (sharedPreference.distanceUnit)
        "${nearestAirport.second?.convertMToMiles()} mi"
    else "${nearestAirport.second?.convertMToKm()} km"

    val scrollState = rememberScrollState()

    Scaffold(topBar = topBar) {
        Column {
            Image(
                painter = painterResource(id = randomDrawableId),
                modifier = Modifier.height(300.dp),
                contentScale = ContentScale.Crop,
                contentDescription = ""
            )
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .verticalScroll(scrollState)
            ) {
                Text(
                    text = stringResource(id = R.string.airport_info),
                    style = TextStyle(
                        fontSize = 26.sp
                    ),
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = selectedAirport?.id ?: "",
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = selectedAirport?.latitude.toString(),
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = selectedAirport?.longitude.toString(),
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = selectedAirport?.name.toString(),
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = selectedAirport?.city ?: "",
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = selectedAirport?.countryId ?: "",
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Spacer(modifier = Modifier.padding(top = 20.dp))
                Text(
                    text = stringResource(id = R.string.nearest_airport),
                    style = TextStyle(
                        fontSize = 26.sp
                    ),
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = nearestAirport.first ?: "",
                    modifier = Modifier.padding(bottom = 10.dp)
                )
                Text(
                    text = stringResource(id = R.string.distance) + " $nearestAirportDistance",
                    modifier = Modifier.padding(bottom = 10.dp)
                )

            }
        }
    }
}
