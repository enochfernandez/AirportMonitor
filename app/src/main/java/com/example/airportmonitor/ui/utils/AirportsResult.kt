package com.example.airportmonitor.ui.utils

import com.example.airportmonitor.model.Airport

/**
 * Holds data for the success or failure of Airports data retrieval
 */
data class AirportsResult (
    val success: List<Airport>? = null,
    val errorOrFailure: String? = null
)
