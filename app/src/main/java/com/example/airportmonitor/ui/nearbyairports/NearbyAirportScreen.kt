package com.example.airportmonitor.ui.nearbyairports

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.airportmonitor.R
import com.example.airportmonitor.core.MySharedPreference
import com.example.airportmonitor.core.convertMToKm
import com.example.airportmonitor.core.convertMToMiles
import com.example.airportmonitor.ui.utils.AirportDataTransformPresenter

@Composable
fun NearbyAirportScreen(
    presenter: AirportDataTransformPresenter,
    sharedPreference: MySharedPreference,
    bottomBar: @Composable () -> Unit,
    topBar: @Composable () -> Unit
) {
    val listOfNearByAirports = presenter.reachMeFromAirport()
        ?.toList()?.sortedBy { it.second.second } ?: emptyList()

    val distancePrefUnit = sharedPreference.distanceUnit

    Scaffold(topBar = topBar, bottomBar = bottomBar) {

        Column {
            Text(
                text = stringResource(id = R.string.nearest_airport_from),
                style = TextStyle(
                    fontSize = 20.sp
                ),
                modifier = Modifier.padding(bottom = 10.dp, start = 20.dp)
            )
            LazyColumn(modifier = Modifier.fillMaxHeight()) {
                items(listOfNearByAirports) { item ->
                    CardDescription(item.second, distancePrefUnit)
                }
            }
        }
    }
}

@Composable
fun CardDescription(item: Pair<String?, Float>, distancePrefUnit: Boolean) {
    val nearestDistanceUnit = if (distancePrefUnit)
        "${item.second.convertMToMiles()} mi"
    else "${item.second.convertMToKm()} km"

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp),
        elevation = 10.dp
    ) {
        Column(
            modifier = Modifier.padding(15.dp)
        ) {
            Text(
                text = item.first ?: "",
                style = TextStyle(
                    fontSize = 26.sp
                ),
                modifier = Modifier.padding(bottom = 10.dp)
            )
            Text(
                text = nearestDistanceUnit,
                modifier = Modifier.padding(bottom = 10.dp)
            )
        }
    }
}
