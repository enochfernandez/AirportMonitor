package com.example.airportmonitor.ui.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector

sealed class Screens(val route: String, val label: String, val icon: ImageVector? = null) {
    object AirportMapsScreen : Screens("AirportMaps", "AirportMaps", Icons.Default.LocationOn)
    object NearbyAirportScreen : Screens("NearbyAirport", "NearbyAirport", Icons.Default.Info)
    object SettingsScreen : Screens(" Settings", " Settings",  Icons.Default.Settings)
    object AirportDetailScreen : Screens("AirportDetail", "AirportDetail")
}
