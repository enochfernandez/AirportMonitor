package com.example.airportmonitor.ui.utils

import android.location.Location
import com.example.airportmonitor.model.Airport
import com.example.airportmonitor.core.Point
import com.example.airportmonitor.core.RotatingCaliper
import com.example.airportmonitor.ui.viewModels.AirportsViewModel
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

/**
 * Presenter class working on data for screens
 */
@ActivityScoped
class AirportDataTransformPresenter @Inject constructor(private val viewModel: AirportsViewModel) {

    init {
        viewModel.getAllAirports()
        viewModel.getAllFlights()
    }

    fun getAirports() = viewModel.getAirportResultValue().success
    private fun getflights() = viewModel.getFlightResultValue().success

    fun getSingleAirport(airportId: String): Airport? {
        return getAirports().let { airports ->
            airports?.find {
                it.id == airportId
            }
        }
    }

    /**
     * Get the nearest airport from the given airport with the airports id
     */
    fun getNearestAirportInfo(airportId: String) : Pair<String?, Float?> {
        val currentAirport = getAirports()?.find { it.id == airportId }
        val otherAirports = getAirports()?.minus(currentAirport)

        val nearestAirport = otherAirports?.minByOrNull {
            currentAirport.distanceFromSelectedAirport(it)
        }

        return Pair(nearestAirport?.name, currentAirport.distanceFromSelectedAirport(nearestAirport))
    }

    /**
     * Return all airports that can be reached from the given airport
     * @param selectedAirportId - airport Id to be used as the selected airport
     */
    fun reachMeFromAirport(selectedAirportId: String = "AMS"): Map<String, Pair<String?, Float>>? {
        val flights = getflights()
        val airports = getAirports()

        val selectedAirport = airports?.find { it.id == selectedAirportId }

        val flightsDepartingFromAms = flights?.filter {
            it.departureAirportId == selectedAirportId
        }?.distinctBy { it.arrivalAirportId }?.map { it.arrivalAirportId }


        val airportsToBeReached = airports?.filter { item ->
            flightsDepartingFromAms?.contains(item.id) ?: false
        }

        return airportsToBeReached?.toList()?.associate {
            it.id to Pair(
                it.name,
                selectedAirport.distanceFromSelectedAirport(it)
            )
        }
    }

    /**
     * Find the furthest points on a plane using the rotatingCaliper algorithm
     */
    fun getFarthestPointsOnMap() : Triple<Point, Point, Double>? {
        val airports = getAirports()?.map { Point(it.latitude, it.longitude, it.id) }?.toTypedArray()
        val rotatingCaliper = RotatingCaliper()
        val convexHull = airports?.let { rotatingCaliper.convexHull(it) }
        return convexHull?.toTypedArray()?.let { rotatingCaliper.rotatingCaliper(it) }
    }
}

fun Airport?.distanceFromSelectedAirport(destinationAirport: Airport?): Float {
    val selectedAirportLocation = Location(this?.name)
    selectedAirportLocation.latitude = this?.latitude ?: 0.0
    selectedAirportLocation.longitude = this?.longitude ?: 0.0

    val destinationAirportLocation = Location(destinationAirport?.name)
    destinationAirportLocation.latitude = destinationAirport?.latitude ?: 0.0
    destinationAirportLocation.longitude = destinationAirport?.longitude ?: 0.0

    return selectedAirportLocation.distanceTo(destinationAirportLocation)
}
