package com.example.airportmonitor

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.airportmonitor.core.MySharedPreference
import com.example.airportmonitor.core.listOfScreens
import com.example.airportmonitor.ui.airportDetail.AirportDetailScreen
import com.example.airportmonitor.ui.airportMaps.AirportMapsScreen
import com.example.airportmonitor.ui.nearbyairports.NearbyAirportScreen
import com.example.airportmonitor.ui.utils.Screens
import com.example.airportmonitor.ui.settings.SettingsScreen
import com.example.airportmonitor.ui.theme.AirportMonitorTheme
import com.example.airportmonitor.ui.utils.AirportDataTransformPresenter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * MainActivity is Base entry point into the application.
 */
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var airportDataTransformPresenter: AirportDataTransformPresenter

    @Inject
    lateinit var sharedPreference: MySharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AirportMonitorTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    MainAppScreen(airportDataTransformPresenter, sharedPreference)
                }
            }
        }
    }
}

@Composable
fun TopBar() {
    TopAppBar(
        title = { Text(text = stringResource(R.string.app_name), fontSize = 18.sp) },
        backgroundColor = MaterialTheme.colors.primary,
        contentColor = Color.White
    )
}

@Composable
fun MainAppScreen(
    airportDataTransformPresenter: AirportDataTransformPresenter,
    sharedPreference: MySharedPreference
) {
    val navController = rememberNavController()
    val bottomBar: @Composable () -> Unit = { AirportMonitorBottomNavigationBar(navController, listOfScreens) }
    val topBar:  @Composable () -> Unit = { TopBar() }

    MainAppLayoutNavigation(navController, airportDataTransformPresenter, sharedPreference, bottomBar, topBar)
}


@Composable
fun MainAppLayoutNavigation(
    navController: NavHostController,
    airportDataTransformPresenter: AirportDataTransformPresenter,
    sharedPreference: MySharedPreference,
    bottomBar: @Composable () -> Unit,
    topBar: @Composable () -> Unit
) {
    NavHost(navController, startDestination = Screens.AirportMapsScreen.route) {
        composable(Screens.AirportMapsScreen.route) {
            AirportMapsScreen(navController, airportDataTransformPresenter, sharedPreference, bottomBar, topBar)
        }
        composable(
            Screens.AirportDetailScreen.route + "/{airportId}",
            arguments = listOf(navArgument("airportId") { type = NavType.StringType })
        ) {
            AirportDetailScreen(
                airportDataTransformPresenter,
                it.arguments?.getString("airportId") ?: "",
                sharedPreference,
                topBar
            )
        }
        composable(Screens.SettingsScreen.route) {
            SettingsScreen(sharedPreference, bottomBar, topBar)
        }
        composable(Screens.NearbyAirportScreen.route) {
            NearbyAirportScreen(airportDataTransformPresenter, sharedPreference, bottomBar, topBar)
        }
    }
}

@Composable
fun AirportMonitorBottomNavigationBar(navController: NavHostController, items: List<Screens>) {
    BottomNavigation (
        backgroundColor = MaterialTheme.colors.primary,
        contentColor = Color.White
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route
        
        items.forEach { screen ->
            BottomNavigationItem(
                icon = { screen.icon?.let { Icon(it, contentDescription = screen.label) } },
                label = { Text(text = screen.label )},
                selected = currentRoute == screen.route,
                alwaysShowLabel = true,
                onClick = {
                    navController.navigate(screen.route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        navController.graph.startDestinationRoute?.let { route ->
                            popUpTo(route) {
                                saveState = true
                            }
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                },
            )
        }
    }
}
