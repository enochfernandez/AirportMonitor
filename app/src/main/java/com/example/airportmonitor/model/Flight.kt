package com.example.airportmonitor.model

import com.squareup.moshi.JsonClass

/**
 * Data class for Flight response
 */
@JsonClass(generateAdapter = true)
data class Flight(
    val airlineId: String,
    val flightNumber: Int,
    val departureAirportId: String,
    val arrivalAirportId: String,
)
