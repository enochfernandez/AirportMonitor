package com.example.airportmonitor.model

import com.squareup.moshi.JsonClass

/**
 * Data class for Airport response
 */
@JsonClass(generateAdapter = true)
data class Airport (
    val id: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    var city: String,
    val countryId: String
)
