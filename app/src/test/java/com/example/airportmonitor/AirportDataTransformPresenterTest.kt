package com.example.airportmonitor

import com.example.airportmonitor.model.Airport
import com.example.airportmonitor.model.Flight
import com.example.airportmonitor.ui.airport.AirportDataTransformPresenter
import com.example.airportmonitor.ui.viewModels.AirportsViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import org.mockito.Mockito.doNothing
import org.mockito.Mockito.mock
import org.mockito.Mockito.spy
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AirportDataTransformPresenterTest {

    private lateinit var spyAirportDataTransformPresenter: AirportDataTransformPresenter

    var mockedAirportViewModel: AirportsViewModel = mock(AirportsViewModel::class.java)

    private lateinit var airportResult: AirportsViewModel.AirportsResult

    private lateinit var flightResult: AirportsViewModel.FlightsResult

    @Before
    fun setUp() {
        airportResult = AirportsViewModel.AirportsResult(airports)
        flightResult = AirportsViewModel.FlightsResult(flights)

        `when`(mockedAirportViewModel.getAirportResultValue()).thenReturn(airportResult)
        `when`(mockedAirportViewModel.getFlightResultValue()).thenReturn(flightResult)
        doNothing().`when`(mockedAirportViewModel).getAllAirports()
        doNothing().`when`(mockedAirportViewModel).getAllFlights()

        spyAirportDataTransformPresenter = spy(
            AirportDataTransformPresenter(mockedAirportViewModel)
        )
    }

    @Test
    fun testGetNearestAirportInfo() {
        //create objects tasks
        val selectedAirportId = "AGP"
        val closestAirportName = "Aalborg Airport"

        //call function
        val result = spyAirportDataTransformPresenter.getNearestAirportInfo(selectedAirportId)

        //check results
        verify(spyAirportDataTransformPresenter).getNearestAirportInfo(anyString())
        assert(result.first == closestAirportName)

    }

    @Test
    fun testGetSingleAirport() {
        //create objects tasks
        val selectedAirportId = "AGP"

        // selectedAirportDetails
        val id = "AGP"
        val latitude = 36.675182
        val longitude = -4.489616
        val name = "Malaga Airport"
        val city = "Malaga"
        val country = "ES"

        //call function
        val result = spyAirportDataTransformPresenter.getSingleAirport(selectedAirportId)

        //check results
        verify(spyAirportDataTransformPresenter).getSingleAirport(anyString())
        assert(result?.id == id)
        assert(result?.latitude == latitude)
        assert(result?.longitude == longitude)
        assert(result?.name == name)
        assert(result?.city == city)
        assert(result?.countryId == country)

    }

    @Test
    fun testReachMeFromAirport() {
        //create objects tasks
        val selectedAirportId = "AGP"

        //call function
        spyAirportDataTransformPresenter.reachMeFromAirport(selectedAirportId)

        //check results
        verify(spyAirportDataTransformPresenter).reachMeFromAirport(anyString())
    }

    companion object {
        val airports = listOf(
            Airport(
                "AGP",
                36.675182,
                -4.489616,
                "Malaga Airport",
                "Malaga", "ES"),
            Airport("AAL",
                57.08655,
                9.872241,
                "Aalborg Airport",
                "Aalborg",
                "DK"),
            Airport( "ABQ",
                35.049625,
                -106.617195,
                "Albuquerque International Airport",
                "Albuquerque",
                "US"),
            Airport("ABZ",
                57.200253,
                -2.204186,
                "Dyce Airport",
                "Aberdeen",
                "GB"),
            Airport("ACC",
                5.60737,
                -0.171769,
                "Kotoka Airport",
                "Accra",
                "GH")
        )

        val flights = listOf(
            Flight( "3O",
                2128,
                "AMS",
                "TNG"),
            Flight( "AF",
                1141,
                "AMS",
                "CDG"),
            Flight( "AF",
                1241,
                "AMS",
                "CDG"),
            Flight("AF",
                1341,
                "AMS",
                "CDG"),
            Flight("AF",
                1441,
                "AMS",
                "CDG")

        )
    }
}
