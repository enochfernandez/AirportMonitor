package com.example.airportmonitor

import com.example.airportmonitor.core.Point
import com.example.airportmonitor.core.RotatingCaliper
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/**
 * Test RotatingCaliper class used to find
 */
class RotatingCaliperTest {

    private lateinit var rotatingCaliper: RotatingCaliper
    private lateinit var points: Array<Point>


    @Before
    fun setup() {
        rotatingCaliper = RotatingCaliper()
        points = arrayOf(
            Point(16.0,  3.0), Point(12.0, 17.0), Point( 0.0,  6.0), Point(-4.0, -6.0),
            Point(16.0,  6.0), Point(16.0, -7.0), Point(16.0, -3.0), Point(17.0, -4.0),
            Point( 5.0, 19.0), Point(19.0, -8.0), Point( 3.0, 16.0), Point(12.0, 13.0),
            Point( 3.0, -4.0), Point(17.0,  5.0), Point(-3.0, 15.0), Point(-3.0, -9.0),
            Point( 0.0, 11.0), Point(-9.0, -3.0), Point(-4.0, -2.0), Point(12.0, 10.0)
        )
    }

    @Test
    fun test_convexHull() {
        //create objects tasks

        //call function
         val convexHull = rotatingCaliper.convexHull(points).toTypedArray()

        //check results
        val actuals = arrayOf(Point(-9.0,-3.0), Point(-3.0,-9.0), Point(19.0, -8.0),
        Point(17.0, 5.0), Point(12.0, 17.0), Point(5.0, 19.0), Point(-3.0, 15.0))
        assertArrayEquals(convexHull, actuals)
    }

    @Test
    fun test_rotatingCaliper() {
        //create objects tasks
        val convexHull = arrayOf(Point(-9.0,-3.0), Point(-3.0,-9.0), Point(19.0, -8.0),
            Point(17.0, 5.0), Point(12.0, 17.0), Point(5.0, 19.0), Point(-3.0, 15.0))

        //call function
        val rotatingCaliper = rotatingCaliper.rotatingCaliper(convexHull)

        //check results
        val actualFurthestPoint1 = Point(19.0, -8.0)
        val actualFurthestPoint2 = Point(-9.0, -3.0)
        val actualDistance = 31.827660925679098

        assertEquals(rotatingCaliper?.first, actualFurthestPoint1)
        assertEquals(rotatingCaliper?.second, actualFurthestPoint2)
        assertEquals(rotatingCaliper?.third, actualDistance)
    }

}
